{** block-description:description **}

{if $product.full_description}
    <div {live_edit name="product:full_description:{$product.product_id}"}>{$product.full_description nofilter}</div>
{else if $product.short_description}
    <div {live_edit name="product:short_description:{$product.product_id}"}>{$product.short_description nofilter}</div>
{/if}

<br />

{if $product.product_condition}
    <table class="ty-table" style='table-layout: fixed; margin-top: 0px'>
        {if $product.ean_upc_isbn}
            <tr>
                <th width='20%'>{__("ean_upc_isbn")}</th>
                <td>{$product.ean_upc_isbn}</td>
            </tr>
        {/if}
        {if $product.package_contents}
            <tr>
                <th width='20%'>{__("package_contents")}</th>
                <td>{$product.package_contents}</td>
            </tr>
        {/if}
        {if $product.primary_color}
            <tr>
                <th width='20%'>{__("primary_color")}</th>
                <td>{$product.primary_color}</td>
            </tr>
        {/if}
        {if $product.country_origin}
            <tr>
                <th width='20%'>{__("country_origin")}</th>
                <td>{$product.country_origin}</td>
            </tr>
        {/if}
        <tr>
            <th width='20%'>{__("product_condition")}</th>
            <td>{if $product.product_condition == 'N'}New{else}Refurbished{/if}</td>
        </tr>
        {if $product.warranty_months}
            <tr>
                <th width='20%'>{__("warranty_months")}</th>
                <td>{$product.warranty_months}</td>
            </tr>
        {/if}
        {if $product.warranty_description}
            <tr>
                <th style="vertical-align: top" width='20%'>{__("warranty_description")}</th>
                <td style="word-wrap: break-word">{$product.warranty_description nofilter}</td>
            </tr>
        {/if}
    </table>
{/if}