{if $auth.user_type == "A"}
    {if $view_uri}
        <li>{btn type="list" target="_blank" text=__("preview") href=$view_uri}</li>
        <li class="divider"></li>
    {/if}
{/if}
<li>{btn type="list" text=__("clone") class="cm-post" href="products.clone?product_id=`$id`"}</li>
{if $allow_save}
    <li>{btn type="list" text=__("delete") class="cm-confirm cm-post" href="products.delete?product_id=`$id`"}</li>
{/if}