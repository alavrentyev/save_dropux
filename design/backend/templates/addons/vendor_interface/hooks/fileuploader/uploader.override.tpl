{if $auth.user_type == "V" && $_REQUEST.dispatch == "companies.update"}
<div id="file_uploader_{$id_var_name}">
    <div class="upload-file-section" id="message_{$id_var_name}" title="">
        <p class="cm-fu-file hidden"><i id="clean_selection_{$id_var_name}" alt="{__("remove_this_item")}" title="{__("remove_this_item")}" onclick="Tygh.fileuploader.clean_selection(this.id); {if $multiupload != "Y"}Tygh.fileuploader.toggle_links(this.id, 'show');{/if} Tygh.fileuploader.check_required_field('{$id_var_name}', '{$label_id}');" class="icon-remove-sign cm-tooltip hand"></i>&nbsp;<span></span></p>
        {if $multiupload != "Y"}<p class="cm-fu-no-file {if $images}hidden{/if}">{__("text_select_file")}</p>{/if}
    </div>
    
    {strip}
    <input type="hidden" name="file_{$var_name}" value="{if $image_name}{$image_name}{/if}" id="file_{$id_var_name}" {if $is_image}class="cm-image-field"{/if} />
    <input type="hidden" name="type_{$var_name}" value="{if $image_name}local{/if}" id="type_{$id_var_name}" {if $is_image}class="cm-image-field"{/if} />

    <div class="btn-group {if $multiupload != "Y" && $images}hidden{/if}" id="link_container_{$id_var_name}">

            <a class="btn" disabled="disabled"><span data-ca-multi="Y" {if !$images}class="hidden" {/if}>{$upload_another_file_text|default:__("upload_another_file")}</span><span data-ca-multi="N" {if $images}class="hidden"{/if}>{$upload_file_text|default:__("local")}</span></a>

        {* amaksimov *}
        {if !($auth.user_type == 'V' && $addons.sd_role_restrictions.status == 'A')}
            {if !($hide_server || "RESTRICTED_ADMIN"|defined)}
                <a class="btn" disabled="disabled"  id="server_{$id_var_name}">{__("server")}</a>
            {/if}
            <a class="btn" disabled="disabled"  id="url_{$id_var_name}">{__("url")}</a>
        {/if}
        {* /amaksimov *}

        {if $hidden_name}
            <input type="hidden" name="{$hidden_name}" value="{$hidden_value}">
        {/if}
    </div>

    {if $allowed_ext}
        <p class="mute micro-note">
            {__("text_allowed_to_upload_file_extension", ["[ext]" => $allowed_ext])}
        </p>
    {/if}
    
    {/strip}
</div>
{/if}