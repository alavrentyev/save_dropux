<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

	if ($mode == 'view' && $auth['user_type'] == 'V') {

	$reports = fn_get_order_reports();

	foreach ($reports as $key => $value) {
		if ($value['description'] == 'Top 10 Customers' || $value['description'] == 'Payment Methods' ) {
			unset($reports[$key]);
		}
	}

	foreach ($reports as $key => $value) {
		Registry::set('navigation.dynamic.sections.addon.' . $key, array (
			  'title' => $value['description'],
			  'href' => "sales_reports.view?report_id=$key",
			  'ajax' => false
		));
	}     
		
	$report = Registry::get('view')->getTemplateVars('report');   

	if ($report['description'] == 'Top 10 Customers' || $report['description'] == 'Payment Methods' ) {
		$report = '';
	}

		Tygh::$app['view']->assign('report', $report);      
}
